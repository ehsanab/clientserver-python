__author__ = 'Ehsan'
import socket   #for sockets
import sys  #for exit

class Client:
    def __init__(self,port,ipAddress):
        self.port = port
        self.ipAddress = ipAddress
        self.s = socket.socket()

    def create_socket (self):
        try:
            #create an AF_INET, STREAM socket (TCP)
            """
            Function socket.socket creates a socket and returns a socket descriptor which can be used in other socket related functions
            Address Family : AF_INET (this is IP version 4 or IPv4)
            Type : SOCK_STREAM (this means connection oriented TCP protocol)
                Apart from SOCK_STREAM, there is SOCK_DGRAM which indicates the UDP protocol.
            """
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error, msg:
            print 'Failed to create socket. Error code: ' + str(msg[0]) + ' , Error message : ' + msg[1]
            sys.exit()
        print 'Socket Created'

    def connect_to_socket (self):
        #Connect to remote server
        self.s.connect((self.ipAddress , self.port))
        print 'Socket Connected to ' + self.ipAddress

    def send_data_to_socket(self, message):
        try:
            """  The message is actually an "http command" to fetch the mainpage of a website. """
            self.s.sendall(message)
        except socket.error:
            #Send failed
            print 'Send failed'
            sys.exit()

    def receive_data_from_socket(self):
        reply = self.s.recv(4096)
        print reply

    def close_socket (self):
        """Function close is used to close the socket. """
        self.s.close()


