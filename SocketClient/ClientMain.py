__author__ = 'Ehsan'


#handling errors in python socket programs

import socket   #for sockets
import sys  #for exit
from ClientSocket import Client;

def main():
    #This is how the class ClientSocket.py works.
    client_object = Client(8888, '127.0.0.1')
    client_object.create_socket()
    client_object.connect_to_socket()
    client_object.send_data_to_socket('Hello')
    client_object.receive_data_from_socket()
    client_object.send_data_to_socket('EECE 412')
    client_object.receive_data_from_socket()


# the function below is not a class. The class implementation of it is in ClientSocket.py
def second_main ():
    try:
        """
        Source:
         http://www.binarytides.com/python-socket-programming-tutorial/
        """
        #create an AF_INET, STREAM socket (TCP)
        """
        Function socket.socket creates a socket and returns a socket descriptor which can be used in other socket related functions
        Address Family : AF_INET (this is IP version 4 or IPv4)
        Type : SOCK_STREAM (this means connection oriented TCP protocol)
            Apart from SOCK_STREAM, there is SOCK_DGRAM which indicates the UDP protocol.
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error, msg:
        print 'Failed to create socket. Error code: ' + str(msg[0]) + ' , Error message : ' + msg[1]
        sys.exit();

    print 'Socket Created'

    """
    we need 2 things , IP address and port number to connect to.
    Before connecting to a remote host, its ip address is needed.
    """

    host = 'localhost'
    port = 8888;

    try:
        remote_ip = socket.gethostbyname( host )

    except socket.gaierror:
        #could not resolve
        print 'Hostname could not be resolved. Exiting'
        sys.exit()

    print 'Ip address of ' + host + ' is ' + remote_ip

    #Connect to remote server
    s.connect((remote_ip , port))

    print 'Socket Connected to ' + host + ' on ip ' + remote_ip



    #Send some data to remote server
    message = "GET / HTTP/1.1\r\n\r\n"

    try :
        """  The message is actually an "http command" to fetch the mainpage of a website. """
        s.sendall(message)
    except socket.error:
        #Send failed
        print 'Send failed'
        sys.exit()

    print 'Message send successfully'



    #Now receive data
    reply = s.recv(1024)

    print reply

    """Function close is used to close the socket. """
    s.close()


if __name__ == '__main__':
    main()