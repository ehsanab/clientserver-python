__author__ = 'Ehsan'

import socket
import sys
from ServerSocket import Server
"""
Source:
 http://www.binarytides.com/python-socket-programming-tutorial/
"""

"""
 Servers basically do the following :
1. Open a socket
2. Bind to a address(and port).
3. Listen for incoming connections.
4. Accept connections
5. Read/Send
"""

def main():
    #this is how to use the serverSocket.py
    server_object = Server(8888,'localhost')
    server_object.bind_server_socket()
    server_object.listen_server_socket(10)
    server_object.accept_connection_server_socket()
    server_object.receive_data_from_server_sockets()
    server_object.send_data_from_server_socket('Server received first segment')
    server_object.receive_data_from_server_sockets()
    server_object.send_data_from_server_socket('Server received second segment')


#This is not the class
def second_main():
    HOST = 'localhost'   # Symbolic name meaning all available interfaces
    PORT = 8888 # Arbitrary non-privileged port

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print 'Socket created'

    """
    Function bind can be used to bind a socket to a particular address and port.
    We bind a socket to a particular IP address and a certain port number. By doing
     this we ensure that all incoming data which is directed towards this port
     number is received by this application.
    This makes it obvious that you cannot have 2 sockets bound to the same port (There are exceptions).
    """
    try:
        s.bind((HOST, PORT))
    except socket.error , msg:
        print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()

    print 'Socket bind complete'



    #After binding a socket to a port, Listen for incoming connections
    """
    The parameter of the function listen is called backlog. It controls the number of incoming
     connections that are kept "waiting" if the program is already busy. So by specifying 10, it
     means that if 10 connections are already waiting to be processed, then the 11th connection
     request shall be rejected.
    """
    s.listen(10)
    print 'Socket now listening'

    #wait to accept a connection - blocking call
    conn, addr = s.accept()

    #display client information
    print 'Connected with ' + addr[0] + ':' + str(addr[1])
    #now keep talking with the client
    data = conn.recv(1024)
    print (data)
    conn.sendall(data)

    conn.close()
    s.close()




if __name__ == '__main__':
    main()