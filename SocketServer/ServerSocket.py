from Tkconstants import W

__author__ = 'Ehsan'
import socket   #for sockets
import sys  #for exit

class Server:
    def __init__(self,port,host):
        self.port = port
        self.ipAddress = None
        self.host = host
        self.s = socket.socket()
        self.conn = None
        self.addr = None




    def create_server_socket (self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print 'Socket created'

    def bind_server_socket(self):
        """
        Function bind can be used to bind a socket to a particular address and port.
        We bind a socket to a particular IP address and a certain port number. By doing
         this we ensure that all incoming data which is directed towards this port
         number is received by this application.
        This makes it obvious that you cannot have 2 sockets bound to the same port (There are exceptions).
        """
        try:
            self.s.bind(('localhost', self.port))
        except socket.error , msg:
            print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
            sys.exit()

        print 'Socket bind complete'

    def listen_server_socket (self, wait_time):
        """
        The parameter of the function listen is called backlog. It controls the number of incoming
         connections that are kept "waiting" if the program is already busy. So by specifying 10, it
         means that if 10 connections are already waiting to be processed, then the 11th connection
         request shall be rejected.
        """
        self.s.listen(wait_time)
        print 'Socket now listening'

    def accept_connection_server_socket (self):
        #wait to accept a connection - blocking call
        self.conn, self.addr = self.s.accept()

        #display client information
        print 'Connected with ' + self.addr[0] + ':' + str(self.addr[1])

    def receive_data_from_server_sockets(self):
        data = self.conn.recv(1024)
        print (data)
        return data


    def send_data_from_server_socket (self, data):
        self.conn.sendall(data)

    def close_connections_and_sockets (self):
        self.conn.close()
        self.s.close()




