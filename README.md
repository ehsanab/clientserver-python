# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The project shows the two ways of implementing the client/server codes in python. One of the files implements the client/server using Object Oriented approach (i.e. classes). The other file uses the normal sequential approach. 

### How do I get set up? ###

I have created the project using the Pycharm. So I would recommend to set up your project using Pycharm. 

### Who do I talk to? ###

At this moment, October 10 2014, I am a 4th year computer Engineering student at UBC. 